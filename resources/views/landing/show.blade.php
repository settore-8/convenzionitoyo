<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
   
    <link rel="shortcut icon" href="https://toyo.it/design/toyo/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>{{ $landing->name }} - Toyo Tires</title>

    <script type="text/javascript">
        var _iub = _iub || [];
        _iub.csConfiguration = {"consentOnScroll":false,"lang":"it","siteId":1730289,"cookiePolicyId":28647457, "banner":{ "acceptButtonDisplay":true,"customizeButtonDisplay":true,"acceptButtonColor":"#0073CE","acceptButtonCaptionColor":"white","customizeButtonColor":"#DADADA","customizeButtonCaptionColor":"#4D4D4D","position":"float-bottom-right","textColor":"black","backgroundColor":"white" }};
    </script><script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>

    {!! htmlScriptTagJsApi([
        'action' => 'homepage',
        'callback_then' => 'callbackThen',
        'callback_catch' => 'callbackCatch'
    ]) !!}
        
</head>

<body>
        <nav class="site-header sticky-top py-1 bg-white border-bottom shadow-sm" >
            <div class="container d-flex flex-column flex-md-row justify-content-between">
                <a class="py-2" href="{{ route('landing', ['slug'=> $landing->slug] ) }}">
                        <svg class="d-block mx-auto" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="168px" height="40.777px" viewBox="495.93 115.217 168 40.777" enable-background="new 495.93 115.217 168 40.777" xml:space="preserve">
                            <polygon fill="#0062B0" points="497.432,127.243 495.93,132.781 500.667,132.781 497.76,143.974 505.053,143.974 507.96,132.781    512.674,132.781 514.176,127.243 497.432,127.243   "/>
                            <path fill="#0062B0" d="M524.959,126.779c-4.273,0-7.772,0.423-9.793,3.637c-1.668,2.666-2.654,6.238-2.654,8.629    c0,5.227,3.898,5.392,9.113,5.392c3.357,0,7.777-0.212,9.609-3.124c1.834-2.823,2.845-6.746,2.845-9.138    C534.079,126.943,529.213,126.779,524.959,126.779L524.959,126.779z M525.242,138.395c-0.705,1.275-1.41,1.577-3.123,1.577    c-0.964,0-2.137-0.394-2.137-2.11c0-0.836,0.539-2.716,1.173-3.898c0.846-1.579,1.808-1.648,2.958-1.648    c1.621,0,2.278,0.743,2.278,1.95C526.392,136.144,525.5,137.931,525.242,138.395L525.242,138.395z"/>
                            <polygon fill="#0062B0" points="547.497,127.241 543.196,134.609 543.126,134.609 542.211,127.241 534.373,127.241    537.869,140.15 535.035,143.974 543.625,143.974 555.805,127.241 547.497,127.241   "/>
                            <path fill="#0062B0" d="M564.918,126.779c-4.274,0-7.773,0.423-9.795,3.637c-1.668,2.666-2.654,6.238-2.654,8.629    c0,5.227,3.899,5.392,9.114,5.392c3.356,0,7.774-0.212,9.606-3.124c1.834-2.823,2.846-6.746,2.846-9.138    C574.035,126.943,569.17,126.779,564.918,126.779L564.918,126.779z M565.199,138.395c-0.704,1.275-1.409,1.577-3.123,1.577    c-0.963,0-2.136-0.394-2.136-2.11c0-0.836,0.539-2.716,1.173-3.898c0.846-1.579,1.808-1.648,2.959-1.648    c1.619,0,2.277,0.743,2.277,1.95C566.35,136.144,565.457,137.931,565.199,138.395L565.199,138.395z"/>
                            <polygon fill="#0062B0" points="580.002,127.243 578.504,132.782 583.24,132.782 580.332,143.974 587.625,143.974    590.533,132.782 595.245,132.782 596.748,127.243 580.002,127.243   "/>
                            <polygon fill="#0062B0" points="597.867,127.241 593.43,143.974 600.497,143.974 605.051,127.241 597.867,127.241   "/>
                            <path fill="#0062B0" d="M623.99,130.965c0-2.366-0.683-3.724-3.872-3.724h-13.842l-4.572,16.732h7.156l1.481-5.827l3.475,5.827    h8.377l-4.842-6.865h1.266C621.645,137.109,623.99,135.184,623.99,130.965L623.99,130.965z M614.416,135.394h-3.21l0.892-3.469    h3.122c0.982,0,1.544,0.374,1.544,1.172C616.764,134.174,616.062,135.394,614.416,135.394L614.416,135.394z"/>
                            <polygon fill="#0062B0" points="643.086,132.532 644.472,127.241 627.181,127.241 622.86,143.972 640.13,143.972 641.277,139.509    631.285,139.509 631.684,138.003 641.676,138.003 642.616,134.392 632.619,134.392 633.095,132.532 643.086,132.532   "/>
                            <path fill="#0062B0" d="M655.624,132.07c2.298,0,4.548,0.229,6.827,0.508l1.479-5.168c-3.427-0.516-7.204-0.634-9.551-0.634    c-1.737,0-4.014,0.048-5.589,0.493c-2.812,0.795-4.689,3.722-4.689,7.307c0,2.629,2.37,3.355,4.527,3.355    c4.2,0,4.624,0.16,4.624,0.965c0,1.076-1.642,1.076-2.418,1.076c-2.695,0-5.35-0.203-8.022-0.527l-1.058,4.215    c3.116,0.563,6.311,0.774,10.068,0.774c4.621,0,6.968-0.446,8.423-2.793c0.496-0.799,1.103-2.608,1.103-3.897    c0-3.52-2.348-3.637-5.021-3.637c-3.685,0-4.152-0.184-4.152-0.857C652.174,132.462,652.828,132.07,655.624,132.07L655.624,132.07    z"/>
                        </svg>
                </a>
                <a class="d-none d-md-inline-block btn btn-primary mt-2  mb-2" href="#form">Approfittane ora</a>
            </div>
        </nav>
            <img src="{{ Storage::disk('public')->url('landing/'.$landing->id.'/'.$landing->cover) }}" alt="{{ $landing->name }}" class="img-fluid d-sm-block d-md-none d-lg-none">
        <main id="testatalanding" class="jumbotron" style="background-image: url('{{ Storage::disk('public')->url('landing/'.$landing->id.'/'.$landing->cover) }}')">
                <div class="container">
                <h1 data-aos="fade-right">{{ $landing->name }}</h1>

                @if($landing->subtitle)
                    <h2 data-aos="fade-up">{!! $landing->subtitle !!}</h2>
                    <a href="#istruzioni" class="btn btn-lg btn-primary mt-2" data-aos="zoom-in">Scopri di più</a>
                @endif

                </div>
            </main>
        

            @if($landing->istruzioni)
                <section id="istruzioni">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-sm-8 offset-sm-2 text-center">
                                    <h3 data-aos="fade-up">{!! $landing->istruzioni !!}</h3>
                                </div>
                            </div>
                        </div>
                </section>
            @endif

        
        <section class="container" id="form">
        
            @if ($message = Session::get('msg')) 
                <div class="alert alert-success">
                        <ul>
                            <li>{{ $message }}</li>
                        </ul>
                </div>
            @endif

            @if ($message = Session::get('msgerror')) 
            <div class="alert alert-danger">
                    <ul>
                        <li>{{ $message }}</li>
                    </ul>
            </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (!$message = Session::get('msg')) 

            <form action="{{ route('landing.request', ['slug'=> $landing->slug] ) }}" method="post">

                @csrf
                <input type="hidden" name="landingid" value="{{ $landing->id }}">
                <input type="hidden" name="landingslug" value="{{ $landing->slug }}">
            
                @if($landing->codice)
                <fieldset class="py-3 blue">
                    <legend>Inserisci il codice promo</legend>
                    <div class="row">
                    
                    <div class="col-sm-4 offset-sm-4">
                        <input text="coupon" name="coupon" class="form-control form-control-lg" value="{{ old('coupon') }}"  required>
                        <label><a href="#" data-toggle="modal" data-target="#modalcoupon">Dove lo trovo?</a></label>
                    </div>
            
                    </div>
                    
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </fieldset>
                @endif

                    <fieldset class="py-3">
                        <legend>I tuoi dati</legend>
                    
                        <div class="row">
                            <div class="col-sm-6  col-lg-3">
                                <label>Nome <span class="richiesto">*</span>
                                <input type="text" name="nome" value="{{ old('nome') }}" size="40" class="form-control" aria-invalid="false">
                            </label>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                            <label>Cognome <span class="richiesto">*</span>
                            <input type="text" name="cognome" value="{{ old('cognome') }}" size="40" class="form-control" aria-required="true" aria-invalid="false" required>
                            </label>
                        </div>
                        
                    <div class="col-sm-6  col-lg-3">
                            <label>Cap <span class="richiesto">*</span>
                            <input type="text" name="citta" value="{{ old('citta') }}" size="40" class="form-control" aria-required="true" aria-invalid="false" required>
                            </label>
                        </div>
                        <div class="col-sm-6  col-lg-3">
                            <label>Email <span class="richiesto">*</span>
                            <input type="email" name="email" value="{{ old('email') }}" size="40" class="form-control" aria-required="true" aria-invalid="false" required>
                            </label>
                        </div>
                    
                    </div>
                </fieldset> 
    
            
        <fieldset class="py-3">
                        <legend>La tua auto</legend>
                        
                        <div class="row">
                            <div class="col-sm-4">

                                <div class="row">

                                    <div class="col-12">
                                        <label class="d-block">Tipologia di Veicolo <span class="richiesto">*</span>
                                        <select name="tipologiaveicolo" class="form-control" aria-required="true" aria-invalid="false" required>
                                            <option value="">---</option>
                                            <option value="Vettura" @if (old('tipologiaveicolo') == 'Vettura') selected="selected" @endif>Vettura</option>
                                            <option value="SUV/4x4" @if (old('tipologiaveicolo') == 'SUV/4x4') selected="selected" @endif>SUV/4x4</option>
                                            <option value="Furgone" @if (old('tipologiaveicolo') == 'Furgone') selected="selected" @endif>Furgone</option></select>
                                        </label>
                                    </div>

                                    <div class="col-12">
                                        <label class="d-block">Modello veicolo <span class="richiesto">*</span>
                                        <input type="text" name="modelloveicolo" value="{{ old('modelloveicolo') }}" size="40" class="form-control" aria-required="true" aria-invalid="false" required>
                                    </div>
                    
                                    <div class="col-12">
                                        <label class="d-block">Percorso prevalente
                                        <select name="tipologiapercorso" class="form-control" aria-invalid="false">
                                            <option value="">---</option>
                                            <option value="Urbano" @if (old('tipologiapercorso') == 'Urbano') selected="selected" @endif>Urbano</option>
                                            <option value="Extraurbano" @if (old('tipologiapercorso') == 'Extraurbano') selected="selected" @endif>Extraurbano</option>
                                            <option value="Misto" @if (old('tipologiapercorso') == 'Misto') selected="selected" @endif>Misto</option>
                                        </select>
                                        </label>
                                    </div>

                                    <div class="col-12">
                                        <label class="d-block">Km annui
                                        <select name="kmpercorsi" class="form-control" aria-invalid="false">
                                            <option value="">---</option>
                                            <option value="meno di 10.000"  @if (old('kmpercorsi') == 'meno di 10.000') selected="selected" @endif>meno di 10.000</option>
                                            <option value="da 10.000 a 20.000"  @if (old('kmpercorsi') == 'da 10.000 a 20.000') selected="selected" @endif>da 10.000 a 20.000</option>
                                            <option value="da 20.000 a 30.000"  @if (old('kmpercorsi') == 'da 20.000 a 30.000') selected="selected" @endif>da 20.000 a 30.000</option>
                                            <option value="da 30.000 a 40.000"  @if (old('kmpercorsi') == 'a 30.000 a 40.000') selected="selected" @endif>da 30.000 a 40.000</option>
                                            <option value="più di 40.000"  @if (old('kmpercorsi') == 'più di 40.000') selected="selected" @endif>più di 40.000</option>
                                        </select>
                                        </label>
                                    </div>

                                    <div class="col-12">
                                        <label class="d-block">Stagione <span class="richiesto">*</span>
                                        <select name="stagione" class="form-control" aria-required="true" aria-invalid="false" required>
                                            <option value="">---</option>
                                            <option value="Estate" @if (old('stagione') == 'Estate') selected="selected" @endif>Estate</option>
                                            <option value="Inverno" @if (old('stagione') == 'Inverno') selected="selected" @endif>Inverno</option>
                                            <option value="4 Stagioni" @if (old('stagione') == '4 Stagioni') selected="selected" @endif>4 Stagioni</option>
                                        </select>
                                        </label>
                                    </div>

                                </div>
                        </div>

                    
                    <div class="col-sm-8">
                        <label>Dati pneumatici Asse Anteriore <span class="richiesto">*</span></label>
                        <div class="fieldset_nidificato fieldset_pneumatico">
                        <img src="https://www.mannuccigomme.it/assets/themes/mannuccigommev2/images/misure-pneumatico.png" class="img-fluid rounded">

                        <label class="dato dato-first">Larghezza
                            <input type="text" name="larghezza_ant" value="{{ old('larghezza_ant') }}" size="4" class="form-control" aria-required="true" aria-invalid="false">
                        </label>
                        
                        <label class="dato">Sezione
                        <input type="text" name="sezione_ant" value="{{ old('sezione_ant') }}" size="5" class="form-control" aria-required="true" aria-invalid="false">
                        </label>
                        
                       <label class="dato">Diametro
                            <input type="text" name="diametro_ant" value="{{ old('diametro_ant') }}" size="5" class="form-control" aria-required="true" aria-invalid="false">
                        </label>

                        <label class="dato">ind. Carico
                            <input type="text" name="indcarico_ant" value="{{ old('indcarico_ant') }}" size="5" class="form-control" aria-required="true" aria-invalid="false">
                        </label>
                        <label class="dato dato-last">Velocità
                            <input type="text" name="velocita_ant" value="{{ old('velocita_ant') }}" size="5" class="form-control" aria-required="true" aria-invalid="false">
                        </label>

                        </div>

                        <label>Dati pneumatici Asse Posteriore <small>(SE DIVERSO)</small></label>
                        <div class="fieldset_nidificato">
                        
                        <label class="dato">Larghezza
                            <input type="text" name="larghezza_pos" value="{{ old('larghezza_pos') }}" size="4" class="form-control" aria-invalid="false">
                        </label>
                        
                        <label class="dato">Sezione
                            <input type="text" name="sezione_pos" value="{{ old('sezione_pos') }}" size="5" class="form-control" aria-invalid="false">
                        </label>
                        
                        <label class="dato">Diametro
                            <input type="text" name="diametro_pos" value="{{ old('diametro_pos') }}" size="5" class="form-control" aria-invalid="false">
                        </label>
                        
                        <label class="dato">ind. Carico
                            <input type="text" name="indcarico_pos" value="{{ old('indcarico_pos') }}" size="5" class="form-control" aria-invalid="false">
                        </label>
                        
                        <label class="dato">Velocità
                            <input type="text" name="velocita_pos" value="{{ old('velocita_pos') }}" size="5" class="form-control" aria-invalid="false">
                        </label>
                        
                    </div>
                    </div>
                        
                   </fieldset>

                        <div class="form-check mb-4">
                                <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="consenso">
                                <label class="form-check-label" for="defaultCheck1">
                                        Confermo di aver letto e compreso la <a href="https://www.iubenda.com/privacy-policy/28647457" class="iubenda-nostyle no-brand iubenda-embed" title="Privacy Policy ">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script> ed autorizzo al trattamento dei miei dati
                                </label>
                        </div>

                        <div class="mt-2 mb-4 from-group text-right">
                        <input type="submit" value="Richiedi preventivo" class="btn btn-lg btn-primary">
                        </div>

                    </form>

                    @endif

        </section>


        @section('toyo')

            <section class="jumbotron"  id="toyo">
                    <div class="container">
                        <div class="row">

                            <div class="col-md-5 offset-md-1">
                                <img src="{{ asset('images/toyo-nel-mondo.jpg') }}" alt="Toyo nel mondo" class="img-fluid">
                            </div>

                            <div class="col-sm-10 offset-sm-1 col-md-6 offset-md-0">
                            <h3>Toyo Tires, <strong>una leadership<br/>
                                conquistata sul campo</strong></h3>
                                <p>Fondata nel 1946 a Osaka, in Giappone, Toyo Tire Corporation
                                        è <strong>una delle più grandi aziende produttrici di pneumatici al mondo</strong>.
                                        Esporta in 80 paesi e presidia i mercati
                                        con un’organizzazione ramificata e una distribuzione puntuale.
                                        Il Centro Tecnico giapponese di Itami sviluppa sofisticati sistemi di analisi:
                                        le <strong>rivoluzionarie simulazioni computerizzate, brevettate in esclusiva,
                                        sono garanzia di leadership tecnologica</strong>.
                                </p>
                                    <a href="https://www.toyo.it" target="_blank" class="btn btn-secondary">Visita il sito</a>
                                    <a href="https://www.facebook.com/ToyoTiresItalia/" target="_blank" class="btn btn-primary">Seguici su Facebook</a>
                            </div>
                        </div>
                </div>
            </section>

        @show

       

        <footer class="bd-footer text-muted">
            <div class="container-fluid p-3 p-md-5">
                    <ul class="bd-footer-links">
                            <li><a href="https://www.toyo.it" target="_blank">Toyo Tires</a></li>

                            <li><a href="https://www.iubenda.com/privacy-policy/28647457" class="iubenda-nostyle no-brand iubenda-embed" title="Privacy Policy ">Privacy Policy</a></li>
                            <li> <a href="https://www.iubenda.com/privacy-policy/28647457/cookie-policy" class="iubenda-nostyle no-brand iubenda-embed" title="Cookie Policy ">Cookie Policy</a></li>
                            <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
                    </ul>
                    <div class="text-muted text-center"><small>© 2019, Toyo Tire Italia S.p.A.<br/>
                        via Napoli, 33, 57014 Collesalvetti, Livorno - tel. 0586.962243 / 963800 - e-mail toyo@toyo.it</small><br/><br/>
                    </div>
            </div>
        </footer>
    
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
          AOS.init();
        </script>


        <div id="modalcoupon" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="Coupon" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                <div class="modal-content p-3">
                    Il codice promo è un codice riservato ai destinatari della convenzione {{ $landing->name }}. Potresti aver ricevuto il codice promo attraverso una newsletter.
                </div>
        </div>

</div>

        
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112130755-8"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112130755-8', { 'anonymize_ip': true });
    </script>

</body>

</html>