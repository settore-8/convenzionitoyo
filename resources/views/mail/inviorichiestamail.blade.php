<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important;
            }
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }
            table[class=body] .content {
                padding: 0 !important;
            }
            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }
            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }
            table[class=body] .btn table {
                width: 100% !important;
            }
            table[class=body] .btn a {
                width: 100% !important;
            }
            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }
        /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
        
        @media all {
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }
    </style>
</head>

<body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">

        <tr>
            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
            <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
                <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;"></span>
                    <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                    <tr>
                                        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                    Gentile Cliente,<br/>
                                                    Toyo Tires ha stretto una convenzione con la Federazione Italiana di Atletica Leggera (FIDAL) rivolta ai suoi tesserati.
                                                    Le giriamo volentieri richiesta di preventivo da parte del tesserato
                                            </p>
                                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                    Nome: <strong>{{ $request->nome }}</strong><br/>
                                                    Cognome: <strong>{{ $request->cognome }}</strong><br/>
                                                    Città/Cap: <strong>{{ $request->citta }}</strong><br/>
                                            </p>
                                            <h3 style="font-family: sans-serif; font-size: 16px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><em>Auto</em></h3>

                                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                    Veicolo: <strong>{{ array_key_exists('veicolo', $request->dati) ? $request->dati['veicolo'] : '-'}}</strong><br/>
                                                    Modello: <strong>{{ array_key_exists('modelloveicolo', $request->dati) ? $request->dati['modelloveicolo'] : '-'}}</strong><br/>
                                                    Percorso: <strong>{{ array_key_exists('percorso', $request->dati) ? $request->dati['percorso'] : '-'}}</strong><br/>
                                                    Km annui: <strong>{{ array_key_exists('km', $request->dati) ? $request->dati['km'] : '-'}}</strong><br/>
                                                    Stagione: <strong>{{ array_key_exists('stagione', $request->dati) ? $request->dati['stagione'] : '-'}}</strong><br/>
                                            </p>

                                            <h3 style="font-family: sans-serif; font-size: 16px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><em>Pneumatici Anteriori</em></h3>

                                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                    Larghezza: <strong>{{ array_key_exists('larghezza_ant', $request->dati) ? $request->dati['larghezza_ant'] : '-'}}</strong><br/>
                                                    Sezione: <strong>{{ array_key_exists('sezione_ant', $request->dati) ? $request->dati['sezione_ant'] : '-'}}</strong><br/>
                                                    Diametro: <strong>{{ array_key_exists('diametro_ant', $request->dati) ? $request->dati['diametro_ant'] : '-'}}</strong><br/>
                                                    Indice di carico: <strong>{{ array_key_exists('indcarico_ant', $request->dati) ? $request->dati['indcarico_ant'] : '-'}}</strong><br/>
                                                    velocità: <strong>{{ array_key_exists('velocita_ant', $request->dati) ? $request->dati['velocita_ant'] : '-'}}</strong><br/>
                                            </p>

                                            @if(array_key_exists('larghezza_pos', $request->dati))
                                                         <h3 style="font-family: sans-serif; font-size: 16px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><em>Pneumatici posteriori</em></h3>

                                                         Larghezza: <strong>{{ array_key_exists('larghezza_pos', $request->dati) ? $request->dati['larghezza_pos'] : '-'}}</strong><br/>
                                                         Sezione: <strong>{{ array_key_exists('sezione_pos', $request->dati) ? $request->dati['sezione_pos'] : '-'}}</strong><br/>
                                                         Diametro: <strong>{{ array_key_exists('diametro_pos', $request->dati) ? $request->dati['diametro_pos'] : '-'}}</strong><br/>
                                                         Indice di carico: <strong>{{ array_key_exists('indcarico_pos', $request->dati) ? $request->dati['indcarico_pos'] : '-'}}</strong><br/>
                                                         velocità: <strong>{{ array_key_exists('velocita_pos', $request->dati) ? $request->dati['velocita_pos'] : '-'}}</strong><br/>

                                            @endif

                                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px; margin-top: 30px">
                                                    Per una maggiore facilità, la preghiamo di compilare il modulo allegato con tutte le informazioni.
                                            </p>
                                                    <ul>
                                                        <li>Prezzo per 4 pneumatici</li>
                                                        <li>Montaggio</li>
                                                        <li>Equilibratura</li>
                                                        <li>Convergenza (eventuale)</li>
                                                        <li>Stoccaggio (eventuale)</li>
                                                        <li>PFU</li>
                                                    </ul>
                                            
                                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                Prego indicare tutti gli importi IVA inclusa
                                            </p>
                                             <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                Grazie della collaborazione e buon lavoro!<br/>
                                                Staff Toyo Tires
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>

                    <!-- START FOOTER -->
                    <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                            <tr>
                                <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">
                                            <img src="#" width="150px" height="17px" alt="Toyo Tires"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Toyo Tire Italia S.p.a.<br/>
                                    via Napoli n. 33 57014 Collesalvetti (Li)</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- END FOOTER -->

                    <!-- END CENTERED WHITE CONTAINER -->
                </div>
            </td>
            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        </tr>
    </table>
</body>

</html>

