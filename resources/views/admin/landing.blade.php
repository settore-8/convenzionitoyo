@extends('layouts.admin')

@section('title')landing page @endsection
@section('pagetitle')Landing Page @endsection


@section('rightmenu')
    <div class="btn-group mr-2">
        <a href="" class="btn btn-sm btn-primary">Nuova convenzione</a>
    </div>
@endsection


@section('content')

<div class="table-responsive">
<table class="table table-striped table-sm">
    <thead>
    <tr>
        <th>Nome</th>
        <th>Slug</th>
        <th>Codice</th>
        <th>Inizio</th>
        <th>Fine</th>
    </tr>
    </thead>
    <tbody>
        @forelse($landings as $l)
        <tr>
            <td><a href="{{ route('admin.landing.edit', $l->id) }}">{{ $l->name }}</a> <a href="{{ route('landing', $l->slug) }}" class="text-muted" target="_blank">Anteprima</a></td>
            <td>{{ $l->slug }}</td>
            <td>{{ $l->codice }}</td>
            <td>{{ $l->start_at }}</td>
            <td>{{ $l->finish_at }}</td>
        </tr>
        @empty

            nessun risultato
        @endforelse
    </tbody>
</table>
</div>

@endsection