@extends('layouts.admin')

@section('title')landing page @endsection
@section('pagetitle')Modifica {{ $landing->name }} @endsection


@section('rightmenu')
    <div class="btn-group mr-2">
        <a href="{{ route('landing', $landing->slug) }}" target="_target" class="btn btn-sm btn-success">Anteprima</a>
    </div>
@endsection


@section('content')


    @if ($message = Session::get('msg')) 
    <div class="alert alert-success">
            <ul>
                <li>{{ $message }}</li>
            </ul>
    </div>
    @endif

    @if ($message = Session::get('msgerror')) 
    <div class="alert alert-danger">
        <ul>
            <li>{{ $message }}</li>
        </ul>
    </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


<form action="{{ route('admin.landing.update', $landing->id ) }}" method="post" enctype="multipart/form-data">
    @csrf

    <input type="hidden" name="id" value="{{ $landing->id }}">

    <div class="form-group">
    <label for="name">Nome <small>(Titolo)</small></label>
        <input type="text" name="name" class="form-control form-control-lg" value="{{ $landing->name }}">
</div>

<div class="form-group">
    <label for="slug">Slug</label>
        <input type="text" name="slug" class="form-control form-control-lg" value="{{ $landing->slug }}">
</div>

<div class="form-group">
     <label for="codice">Codice promo</label>
    <input type="text" name="codice" class="form-control form-control-lg" value="{{ $landing->codice }}">
</div>

<div class="form-group">
    <label for="subtitle">Sottotitolo</label>
    <textarea type="textarea" name="subtitle" class="form-control form-control-lg" rows="3">{{ $landing->subtitle }}</textarea>
</div>

<div class="form-group">
    <label for="istruzioni">Istruzioni</label>
    <textarea type="textarea" name="istruzioni" class="form-control form-control-lg" rows="3">{{ $landing->istruzioni }}</textarea>
</div>

<div class="form-group row">
    <div class="col-sm-12 col-md-2">
<img src="{{ Storage::disk('public')->url('landing/'.$landing->id.'/'.$landing->cover) }}" class="img-fluid">
    </div>
    <div class="col-sm-12 col-md-10">
    <div class="custom-file">
            <input type="file" class="custom-file-input custom-file-input-lg"  name="cover">
            <label class="custom-file-label form-control-lg" for="cover">Selezione immagine</label>
    </div>
</div>
</div>


<div class="custom-control custom-switch">
    <input type="checkbox" class="custom-control-input" id="customSwitch2">
    <label class="custom-control-label " for="customSwitch2">Sezione istruzioni</label>
</div>

<div class="custom-control custom-switch">
    <input type="checkbox" class="custom-control-input" id="customSwitch1">
    <label class="custom-control-label" for="customSwitch1">Sezione Toyo</label>
</div>

<div class="form-group">
    <button class="btn btn-primary float-right">Salva</button>
</div>
     
</form>

@endsection