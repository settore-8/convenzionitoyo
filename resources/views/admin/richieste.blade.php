@extends('layouts.admin')

@section('title')Richieste @endsection
@section('pagetitle')Richieste <span class="badge badge-pill badge-danger">{{ count($richieste) }}</span> @endsection

@section('rightmenu')
    <!--
    <div class="btn-group mr-2">
        <button type="button" class="btn btn-sm btn-outline-secondary">Esporta</button>
    </div>
      <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
        This week
    </button>
-->
@endsection

@section('content')


@if ($message = Session::get('msg')) 
<div class="alert alert-success">
        <ul>
            <li>{{ $message }}</li>
        </ul>
</div>
@endif

@if ($message = Session::get('msgerror')) 
<div class="alert alert-danger">
    <ul>
        <li>{{ $message }}</li>
    </ul>
</div>
@endif



<div class="table-responsive">
<table class="table table-striped table-sm">
    <thead>
    <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>Cognome</th>
        <th>Città/Cap</th>
        <th>Email</th>
        <th>Tel</th>
        <th>Campagna</th>
        <th>Auto</th>
        <th>Data</th>
       <!-- <th>Action</th>-->
    </tr>
    </thead>
    <tbody>
        @forelse($richieste as $r)
     
        <tr>
            <td>{{ $r->id }}</td> 
            <td>{{ $r->nome }}</td>
            <td>{{ $r->cognome }}</td>
            <td>{{ $r->citta }}</td>
            <td>{{ $r->email }}</td>
            <td>{{ $r->telefono }}</td>
            <td>{{ $r->landing->name }}</td>
            <td>{{ array_key_exists('veicolo', $r->dati) ? $r->dati['veicolo'] : ''}}</td>
            <td>{{ $r->created_at }}</td>
            <!--<td><a href="{{ route('admin.richieste.mail', $r->id ) }}">Invio email</td>-->
        </tr>
        @empty

            nessun risultato
        @endforelse
    </tbody>
</table>
</div>

@endsection