<!doctype html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - Convenzioni Toyo</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
  </head>

  <body class="@yield('bodyclass')">
  @auth
  <nav class="navbar navbar-dark fixed-top bg-blue flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Convenzioni Toyo</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">Logout</a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
        </li>
      </ul>
  </nav>
  @endauth


<div class="container-fluid">
  <div class="row">
      @auth
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link {{  Route::currentRouteName() === "admin.dashboard" ? "active" : "" }}" href="{{ route('admin.dashboard') }}">
              Dashboard 
            </a>
          </li>
          <!--
          <li class="nav-item">
            <a class="nav-link {{  Route::currentRouteName() === "admin.landing" ? "active" : "" }}" href="{{ route('admin.landing') }}">
              Landing
            </a>
          </li>
        -->
          <li class="nav-item">
            <a class="nav-link" {{  Route::currentRouteName() === "admin.richieste" ? "active" : "" }} href="{{ route('admin.richieste') }}">
              Richieste
            </a>
          </li>
          <!--
          <li class="nav-item">
            <a class="nav-link" {{  Route::currentRouteName() === "admin.preventivi" ? "active" : "" }} href="{{ route('admin.preventivi') }}">
              Preventivi
            </a>
          </li>

        -->
        </ul>
     
      </div>
    </nav>
    

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
        <h1 class="h2">@yield('pagetitle')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          @yield('rightmenu')
        </div>
      </div>

      @yield('content')
      
    </main>

    @else
    <main role="main" class="col-lg-12 px-4">
      @yield('content')
    </main>

    @endauth

  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

      </body>
</html>
