<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/pass', function () {
    return bcrypt('settore8');
});
*/

Route::get('/', function () {
    return redirect()->away('https://www.toyo.it');
});

Route::prefix('/landing/{slug}')->group(function(){
    Route::get('/', ['as' => 'landing', 'uses' => 'LandingController@show']);
    Route::post('/store', ['as' => 'landing.request', 'uses' => 'RequestController@store']);
});

Route::prefix('/req/{id}/{token}/')->group(function(){
    Route::get('/', ['as' => 'richiesta', 'uses' => 'RequestController@show']);
    //Route::post('/', ['as' => 'landing.request', 'uses' => 'RequestController@store']);
});


    
Route::prefix('/administration')->group(function(){
    Route::middleware('auth')->group(function(){
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'AdminController@dashboard']);
    // Landing
    Route::get('/landing', ['as' => 'admin.landing', 'uses' => 'AdminController@landing']);
    Route::get('/landing/edit/{id}', ['as' => 'admin.landing.edit', 'uses' => 'AdminController@landingedit']);
    Route::post('/landing/edit/{id}', ['as' => 'admin.landing.update', 'uses' => 'AdminController@landingupdate']);
    // Richieste
    Route::get('/richieste', ['as' => 'admin.richieste', 'uses' => 'AdminController@richieste']);
    Route::get('/richieste/send/{id}', ['as' => 'admin.richieste.mail', 'uses' => 'AdminController@sendRicheistaMailtoToyo']);
    Route::get('/preventivi', ['as' => 'admin.preventivi', 'uses' => 'AdminController@preventivi']);
    });

});

Auth::routes([
    'register' => false, 
    'reset' => false, 
    'verify' => false, 
]);

Route::get('/home', 'HomeController@index')->name('home');
