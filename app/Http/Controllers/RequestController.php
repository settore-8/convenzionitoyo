<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Richieste;
use App\Landing;
use Validator;
use Mail;
use App\Mail\ThankyouMail;
use App\Mail\InvioRichiestaMail;


class RequestController extends Controller
{
    public function create()
    {
        return view('landing.show');
    }

    public function store(Request $request)
    {
        
        Validator::extend('coupon', function ($attribute, $value, $parameters, $validator) use ($request) {
            return Landing::where('codice', $value)->where('id', $request->landingid)->get()->isNotEmpty();
        }, 'Codice non valido');

        Validator::extend('uniqueemail', function ($attribute, $value, $parameters, $validator) use ($request) {
            return Richieste::where('email', $value)->where('landing_id', $request->landingid)->get()->isEmpty();
        }, 'Esiste già una richeista associata a questa email');


        $validatedData = $request->validate([
            'nome' => 'required|max:255',
            'cognome' => 'required|max:255',
            'citta' => 'required',
            'coupon' => 'required|coupon',
            'email' => 'required|uniqueemail|max:255|email',
            'landingid' => 'required',
            'tipologiaveicolo' => 'required',
            'modelloveicolo' => 'required',
            'stagione' => 'required',
            'consenso' => 'required',
            'larghezza_ant' => 'required',
            'sezione_ant' => 'required',
            'diametro_ant' => 'required',
            'indcarico_ant' => 'required',
            'velocita_ant' => 'required'
        ], [
            'nome.required' => 'Il campo Nome è richiesto',
            'nome.max' => 'Il campo Nome è troppo lungo',
            'cognome.required' => 'Il campo Cognome è richiesto',
            'cognome.max' => 'Il campo Cognome è troppo lungo',
            'citta.required' => 'Il campo Cap è richiesto',
            'email.required' => 'Il campo Email è richiesto',
            'email.max' => 'Il campo Email è troppo lungo',
            'email.email' => 'Inserire un indirizzo Email valido',
            'tipologiaveicolo.required' => 'Il campo Tipologia veicolo è richiesto',
            'modelloveicolo.required' => 'Il campo Modello veicolo è richiesto',
            'stagione.required' => 'Il campo Stagione è richiesto',
            'larghezza_ant.required' => 'Per favore compilare il campo Larghezza',
            'sezione_ant.required' => 'Per favore compilare il campo Sezione',
            'diametro_ant.required' => 'Per favore compilare il campo Diametro',
            'indcarico_ant.required' => 'Per favore compilare il campo Indice di Carico',
            'velocita_ant.required' => 'Per favore compilare il campo Indice di Velocità',
            'consenso.required' => 'Per favore, accettare la privacy policy',
        ]);

        try {
            

            $richiesta = Richieste::create([
            'landing_id' => $request->landingid,
            'nome' => $request->nome,
            'cognome' => $request->cognome,
            'citta' => $request->citta,
            'email' => $request->email,
            'dati' => serialize([
                'veicolo' => $request->tipologiaveicolo,
                'modelloveicolo' => $request->modelloveicolo,
                'percorso' => $request->percorso,
                'km' => $request->kmpercorsi,
                'stagione' => $request->stagione,
                'larghezza_ant' => $request->larghezza_ant,
                'sezione_ant' => $request->sezione_ant,
                'diametro_ant' => $request->diametro_ant,
                'indcarico_ant' => $request->indcarico_ant,
                'velocita_ant' => $request->velocita_ant,
                'larghezza_pos' => $request->larghezza_pos,
                'sezione_pos' => $request->sezione_pos,
                'diametro_pos' => $request->diametro_pos,
                'indcarico_pos' => $request->indcarico_pos,
                'velocita_pos' => $request->velocita_pos

            ]),
            'token' => Str::random(256)
        ]);
        

        Mail::to($request->email)->send(new ThankyouMail($richiesta));
        Mail::to(['order@toyo.it', 'marketing@toyo.it'])->send(new InvioRichiestaMail($richiesta, $richiesta->landing->name));
            
        } catch(\Exception $e) {
            return redirect()->route('landing', $request->landingslug)->with('msgerror', 'Errore invio email');
        }
        

        return redirect()->route('landing', $request->landingslug)->with('msg', 'Richiesta inviata');
        
    }

    

    public function show($id, $token) {

        $richiesta = Richieste::where('token',$token)
        ->where('id', $id)->first();
        if(!$richiesta) {
            abort(404, 'La pagina non esiste');
        }
        return view('richiesta.show', compact('richiesta'));

    }


}