<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Richieste;
use App\Landing;
use Mail;
use App\Mail\InvioRichiestaMail;
use Illuminate\Support\Str;


class AdminController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    
    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function landing()
    {
        $landings = Landing::simplePaginate(50);
        return view('admin.landing', compact('landings'));
    }

    public function landingedit($id)
    {
        $landing = Landing::findOrFail($id);
        return view('admin.landingedit', compact('landing'));
    }

    public function landingupdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'id' => 'required',
            'name' => 'required',
            'slug' => 'required',
            'codice' => 'required|max:13',
            'subtitle' => 'max:255',
        ], [
            'id.required' => 'Il campo ID è richiesto',
            'name.required' => 'Il campo Nome è richiesto',
            'slug.required' => 'Il campo Slug è richiesto',
            'codice.max' => 'Il campo Codice non può essere più lungo di 13 caratteri',
            'subtitle.max' => 'Il campo Sottotitolo non può essere più lungo di 255 caratteri',
        ]);

        $landing = Landing::findOrFail($id);

        
        try {

        $landing->update([
            'name' => $request->name,
            'slug' => Str::slug($request->slug, '-'),
            'codice' => strtoupper(Str::slug($request->codice, '')),
            'subtitle' => $request->subtitle,
            'istruzioni' => $request->istruzioni
        ]);


        if($request->hasFile('cover')) {
            $imageName = $request->file('cover')->getClientOriginalName(); 
            $landing->update([
                'cover' => $imageName,
            ]);
            Storage::disk('public')->putFileAs('landing/'.$landing->id, $request->file('cover'), $imageName);
        }
       

        } catch(\Exception $e) {
            return redirect()->route('admin.landing.edit', $request->id)->with('msgerror', 'Errore salvataggio');
        }

        return redirect()->route('admin.landing.edit', $request->id)->with('msg', 'Modifiche salvate');
        
    }

    public function richieste()
    {
        $richieste = Richieste::simplePaginate(50);
        return view('admin.richieste', compact('richieste'));
    }

    public function preventivi()
    {
        return view('admin.preventivi');
    }



    // Mail

    public function sendRicheistaMailtoToyo($id) {

        $richiesta = Richieste::findOrFail($id);
        if(!$richiesta) {
            abort(404, 'La pagina non esiste');
        }

        Mail::to(['order@toyo.it', 'marketing@toyo.it'])->send(new InvioRichiestaMail($richiesta, $richiesta->landing->name));

        return redirect()->route('admin.richieste', $richiesta->id)->with('msg', 'Email inviata');

    }

}
