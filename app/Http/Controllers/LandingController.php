<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Landing;
use Carbon\Carbon;


class LandingController extends Controller
{
    public function show($slug)
    {
        $landing = Landing::where('slug',$slug)
        ->where('finish_at', '>=', Carbon::now())
        ->where('start_at', '<=', Carbon::now())->first();
        if(!$landing) {
            abort(404, 'La pagina non esiste');
        }
        return view('landing.show', compact('landing'));
    }
}
