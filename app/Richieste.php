<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Richieste extends Model
{
    protected $table = 'requests';
    protected $fillable = ['nome','cognome', 'citta', 'email', 'dati', 'landing_id', 'token'];


    public function landing()
    {
        return $this->belongsTo('App\Landing', 'landing_id');
    }

    // mutator

    public function getDatiAttribute($value)
    {
        return !is_null($value) ? unserialize($value) : [];
    }


}
