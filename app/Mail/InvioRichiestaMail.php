<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvioRichiestaMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $landingname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $landingname)
    {
        $this->request = $request;
        $this->landingname = $landingname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->request->email, 'Convenzioni Toyo')
                    ->subject('Richiesta '. $this->landingname.' n° '.$this->request->id. '')
                    ->replyTo('noreply@convenzionitoyo.it', 'Convenzioni Toyo')
                    ->view('mail.inviorichiestamail');
    }
}
