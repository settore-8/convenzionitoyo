<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ThankyouMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@convenzionitoyo.it', 'Convenzioni Toyo')
                    ->subject('Grazie '.$this->request->nome. ', di aver scelto Toyo')
                    ->replyTo('noreply@convenzionitoyo.it', 'Convenzioni Toyo')
                    ->view('mail.thankyoumail');
    }
}
