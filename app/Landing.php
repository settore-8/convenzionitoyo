<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{

    protected $fillable = ['name','slug','subtitle', 'codice', 'cover', 'istruzioni', 'start_at', 'finish_at'];

    public function richieste()
    {
        return $this->hasMany('App\Richieste', 'landing_id');
    }



}
